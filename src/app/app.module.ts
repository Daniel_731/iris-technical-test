import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SharedComponentsModule} from "./shared-components/shared-components.module";
import {RxReactiveFormsModule} from "@rxweb/reactive-form-validators";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatChipsModule} from "@angular/material/chips";


@NgModule({
	declarations: [
		AppComponent
	],
	
	imports: [
		BrowserModule,
		AppRoutingModule,
		SharedComponentsModule,
		RxReactiveFormsModule,
		BrowserAnimationsModule,
		MatChipsModule
	],
	
	providers: [],
	
	bootstrap: [
		AppComponent
	]
})
export class AppModule {
}
