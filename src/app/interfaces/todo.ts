import {Category} from "./category";




/**
 * @description Estructura de un pendiente
 */
export interface Todo {
	id: string;
	category: string;
	name: string;
	status: TodoStatus;
}



/**
 * @description Posibles estados de un pendiente
 */
export enum TodoStatus {
	pending,
	done
}
