
/**
 * @description Estructura de una categoría
 */
export interface Category {
	id: string;
	name: string;
}
