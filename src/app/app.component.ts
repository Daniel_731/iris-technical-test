import {Component, OnInit} from '@angular/core';
import {Todo, TodoStatus} from "./interfaces/todo";
import {RxFormBuilder, RxFormGroup, RxwebValidators} from "@rxweb/reactive-form-validators";
import { v4 as uuidv4 } from 'uuid';
import {SelectOption} from "./shared-components/select/interfaces/select-option";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	
	/** Imágenes **/
	
	public readonly IMAGE__TODO_EMPTY: string;
	public readonly IMAGE__PLUS__BLACK: string;
	
	
	
	/** Variables generales **/
	
	// Formulario
	public todoListForm!: RxFormGroup;
	
	// Lista de pendientes
	public todoList: Todo[];
	
	// Bandera para el primer pendiente
	public isFirstTodo: boolean;
	
	// Opciones a mostrar
	public viewOptions: SelectOption[];
	
	
	
	
	constructor(private _rxFormBuilder: RxFormBuilder) {
		
		/** Imágenes: valores iniciales **/
		this.IMAGE__TODO_EMPTY = 'assets/shared/svg/empty.svg';
		this.IMAGE__PLUS__BLACK = 'assets/shared/png/plus__black.png';
		
		
		/** Variables generales: valores iniciales **/
		this.todoList = [];
		this.isFirstTodo = false;
		this.viewOptions = [];
	}
	
	
	
	
	public ngOnInit(): void {
		
		// Inicio el formulario
		this._initTodoListForm();
		
		// Inicio las opciones de visualización
		this._initViewOptions();
	}
	
	
	
	
	/**
	 * @description Inicia el formulario para crear pendientes
	 *
	 * @private
	 */
	private _initTodoListForm(): void {
		this.todoListForm = <RxFormGroup> this._rxFormBuilder.group({
			
			category: [
				'important',
				[RxwebValidators.required({message: 'Este campo es requerido'})]
			],
			
			name: [
				null,
				[RxwebValidators.required({message: 'Este campo es requerido'})]
			]
		});
	}
	
	
	
	/**
	 * @description Inicia las opciones de visualización
	 *
	 * @private
	 */
	private _initViewOptions(): void {
		this.viewOptions = [
			{
				id: '1',
				value: 'all',
				label: 'All'
			},
			{
				id: '2',
				value: 'done',
				label: 'Done'
			},
			{
				id: '3',
				value: 'pending',
				label: 'Pending'
			}
		];
	}
	
	
	
	/**
	 * @description Limpia los campos del formulario
	 *
	 * @private
	 */
	private _clearTodoListForm(): void {
		this.todoListForm.controls['name'].setValue(null);
	}
	
	
	
	/**
	 * @description Crea un nuevo pendiente
	 */
	public createTodo(): void {
		
		if (this.todoListForm.invalid) { return; }
		
		const category: string = this.todoListForm.controls['category'].value;
		const name: string = this.todoListForm.controls['name'].value;
		
		this.todoList.push({
			id: uuidv4(),
			category,
			name,
			status: TodoStatus.pending
		});
		
		this._clearTodoListForm();
	}
	
	
	
	/**
	 * @description Elimina un item
	 *
	 * @param index
	 */
	public removeItem(index: number): void {
		this.todoList.splice(index, 1);
	}
	
	
	
	public onChangeStatus(): void {
	
	}
	
	
}
