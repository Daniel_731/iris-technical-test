import {Component} from '@angular/core';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
	
	/** Imágenes **/
	
	public readonly IMAGE__IRIS_LOGO: string;
	
	
	
	
	constructor() {
		
		/** Imágenes: valores iniciales **/
		this.IMAGE__IRIS_LOGO = 'assets/shared/png/logo-iris.png';
	}
	
}
