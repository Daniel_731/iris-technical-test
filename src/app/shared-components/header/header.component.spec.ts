import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HeaderComponent} from './header.component';

describe('HeaderComponent', () => {
	let app: HeaderComponent;
	let fixture: ComponentFixture<HeaderComponent>;
	
	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [HeaderComponent]
		})
			.compileComponents();
		
		fixture = TestBed.createComponent(HeaderComponent);
		app = fixture.componentInstance;
		fixture.detectChanges();
	});
	
	it('should create', () => {
		expect(app).toBeTruthy();
	});
	
	
	it('Existencia de una imagen', function () {
		expect(app.IMAGE__IRIS_LOGO).toBeTruthy();
	});
	
});
