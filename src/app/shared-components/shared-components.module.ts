import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SelectComponent} from './select/select.component';
import {HeaderComponent} from "./header/header.component";
import {ButtonComponent} from './button/button.component';
import {InputTextIconComponent} from './input-text-icon/input-text-icon.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TodoItemComponent} from './todo-item/todo-item.component';
import {MatCheckboxModule} from "@angular/material/checkbox";


@NgModule({
	declarations: [
		HeaderComponent,
		SelectComponent,
		ButtonComponent,
		InputTextIconComponent,
		TodoItemComponent
	],
	
	imports: [
		CommonModule,
		ReactiveFormsModule,
		FormsModule,
		MatCheckboxModule
	],
	
	exports: [
		HeaderComponent,
		SelectComponent,
		ButtonComponent,
		InputTextIconComponent,
		TodoItemComponent
	]
})
export class SharedComponentsModule {
}
