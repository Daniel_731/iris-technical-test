import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
	selector: 'app-button',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
	
	/** Inputs */
	
	// Label
	@Input() label!: string;
	
	
	
	/** Outputs */
	@Output() outputOnClick: EventEmitter<any>;
	
	
	
	
	constructor() {
		
		/** Outputs: valores iniciales */
		this.outputOnClick = new EventEmitter<any>();
	}
	
}
