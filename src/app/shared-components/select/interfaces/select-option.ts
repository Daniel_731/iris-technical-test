
/**
 * @description Estructura de un item del select
 */
export interface SelectOption {
	id: string;
	label: string;
	value: string;
}
