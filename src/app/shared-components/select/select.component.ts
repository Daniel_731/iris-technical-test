import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SelectOption} from "./interfaces/select-option";
import { v4 as uuidv4 } from 'uuid';
import {RxFormGroup} from "@rxweb/reactive-form-validators";
import {AbstractControl} from "@angular/forms";

@Component({
	selector: 'app-select',
	templateUrl: './select.component.html',
	styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
	
	/** Inputs */
	
	// Formulario
	@Input() rxFormGroup!: RxFormGroup;
	
	// Nombre del campo
	@Input() formControlString!: string;
	
	// Opciones
	@Input() options: SelectOption[];
	
	
	
	/** Output */
	
	// Al cambiar el select
	@Output() outputChangeSelect: EventEmitter<string>;
	
	
	
	/** Imágenes */
	public readonly IMAGE__ARROW_DOWN__BLACK: string;
	
	
	
	/** Variables generales */
	
	// Control del formulario
	public control!: AbstractControl;
	
	// Valor
	public value: string;
	
	
	
	constructor() {
		
		/** Inputs: valores iniciales */
		this.options = [];
		
		
		/** Output: valores iniciales */
		this.outputChangeSelect = new EventEmitter<string>()
		
		
		/** Imágenes: valores iniciales */
		this.IMAGE__ARROW_DOWN__BLACK = 'assets/shared/png/arrow-down__black.png';
		
		
		/** Variables generales: valores iniciales */
		this.value = '';
	}
	
	
	
	
	public ngOnInit(): void {
		
		// Inicio el control
		this._initFormControl();
		
		// Opciones por defecto
		if (!this.options.length) {
			this._optionsDefaultData();
		}
	}
	
	
	
	
	/**
	 * @description Inicia el control del formulario
	 *
	 * @private
	 */
	private _initFormControl(): void {
		if (!!this.formControlString) {
			this.control = this.rxFormGroup?.controls?.[this.formControlString];
		}
	}
	
	
	
	/**
	 * @description Crea las opciones por defecto para el select
	 *
	 * @private
	 */
	private _optionsDefaultData(): void {
		this.options = [
			{
				id: uuidv4(),
				label: 'Important',
				value: 'important'
			},
			{
				id: uuidv4(),
				label: 'Urgent',
				value: 'urgent'
			},
			{
				id: uuidv4(),
				label: 'Optional',
				value: 'optional'
			},
			{
				id: uuidv4(),
				label: 'Not important',
				value: 'not_important'
			}
		];
	}
	
}
