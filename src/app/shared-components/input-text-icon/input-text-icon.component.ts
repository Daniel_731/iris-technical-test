import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RxFormGroup} from "@rxweb/reactive-form-validators";
import {AbstractControl} from "@angular/forms";

@Component({
	selector: 'app-input-text-icon',
	templateUrl: './input-text-icon.component.html',
	styleUrls: ['./input-text-icon.component.scss']
})
export class InputTextIconComponent implements OnInit {
	
	/** Inputs */
	
	// Formulario
	@Input() rxFormGroup!: RxFormGroup;
	
	// Nombre del campo
	@Input() formControlString!: string;
	
	// Placeholder
	@Input() placeholder: string;
	
	// Icono
	@Input() icon!: string;
	
	
	
	/** Outputs */
	
	// Evento de agregar
	@Output() outputAdd: EventEmitter<any>;
	
	
	
	/** Variables generales */
	
	// Control del formulario
	public control!: AbstractControl;
	
	
	
	
	constructor() {
		
		/** Inputs: valores iniciales */
		this.placeholder = 'Placeholder';
		this.icon = 'assets/shared/png/plus__black.png';
		
		
		/** Outputs: valores iniciales */
		this.outputAdd = new EventEmitter<any>();
	}
	
	
	
	
	public ngOnInit(): void {
		
		// Inicio el control
		this._initFormControl();
	}
	
	
	
	
	/**
	 * @description Inicia el control del formulario
	 *
	 * @private
	 */
	private _initFormControl(): void {
		if (!!this.formControlString) {
			this.control = this.rxFormGroup?.controls?.[this.formControlString];
		}
	}
	
}
