import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Todo} from "../../interfaces/todo";

@Component({
	selector: 'app-todo-item',
	templateUrl: './todo-item.component.html',
	styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent {
	
	/** Inputs */
	
	// Item
	@Input() item!: Todo;
	
	// Check
	@Input() isChecked: boolean;
	
	
	
	/** Outputs */
	
	// Eliminar item
	@Output() outputRemoveItem: EventEmitter<any>;
	
	
	
	/** Imágenes */
	
	public readonly IMAGE__CLOSE__RED: string;
	
	
	
	
	constructor() {
		
		/** Inputs: valores iniciales */
		this.isChecked = false;
		
		
		/** Outputs */
		this.outputRemoveItem = new EventEmitter<any>();
		
		
		/** Imágenes */
		this.IMAGE__CLOSE__RED = 'assets/shared/png/close__red.png';
	}
	
	
	
	
	public colorByCategory(): string {
		
		const importantColor: string = '#fff3e0';
		const urgentColor: string = '#ffebee';
		const optionalColor: string = '#e3f2fd';
		const notImportantColor: string = '#FFFFFF';
		
		let color: string = '';
		
		switch (this.item.category) {
			case 'important':
				color = importantColor;
				break;
			
			case 'urgent':
				color = urgentColor;
				break;
			
			case 'optional':
				color = optionalColor;
				break;
			
			case 'not_important':
				color = notImportantColor;
				break;
				
			default:
				break;
		}
		
		return color;
	}
	
}
